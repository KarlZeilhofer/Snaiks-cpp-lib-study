var class_sns_clock_master =
[
    [ "SnsClockMaster", "class_sns_clock_master.html#ac19c2c391132e05706d94554e6f9df99", null ],
    [ "addSlave", "class_sns_clock_master.html#afbb7deca8cfaa3a01a85bfee12333ba1", null ],
    [ "process", "class_sns_clock_master.html#a1c1c294ae8ef35c48e375c7d80ece749", null ],
    [ "processAllSlaves", "class_sns_clock_master.html#a24006b7a887c964a6bebf9e91ae95f47", null ],
    [ "removeSlave", "class_sns_clock_master.html#a30b8ba0f9dd2b45c6d184ff54089b39d", null ],
    [ "reset", "class_sns_clock_master.html#a1d395fb3783f5722e138203c284a960e", null ],
    [ "sample", "class_sns_clock_master.html#a7ce0f4a19d88f4534a2305419be2883f", null ],
    [ "sampleAllSlaves", "class_sns_clock_master.html#ad002ef92cc2d8f51f7fc41f518aa00e3", null ],
    [ "maxNSlaves", "class_sns_clock_master.html#ab1a09530506ab69aee93c83b33cde606", null ],
    [ "nSlaves", "class_sns_clock_master.html#a617a74f2f60d74aaa8aa106881ba5fba", null ],
    [ "pSlaves", "class_sns_clock_master.html#a1a41f510e8b91aa0f9a77a270a463d55", null ]
];