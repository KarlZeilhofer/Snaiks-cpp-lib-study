var hierarchy =
[
    [ "SnsMemoryManager", "class_sns_memory_manager.html", null ],
    [ "SnsSystem", "class_sns_system.html", [
      [ "SnsClockMaster", "class_sns_clock_master.html", [
        [ "SnsClockDevider", "class_sns_clock_devider.html", null ]
      ] ],
      [ "SnsConverter< T_From, T_To >", "class_sns_converter.html", null ],
      [ "SnsHybrid< T1, T2 >", "class_sns_hybrid.html", null ],
      [ "SnsHybrid< T, bool >", "class_sns_hybrid.html", [
        [ "SnsSchmittTrigger< T >", "class_sns_schmitt_trigger.html", null ]
      ] ],
      [ "SnsHybrid< T, T >", "class_sns_hybrid.html", [
        [ "SnsNumeric< T >", "class_sns_numeric.html", [
          [ "SnsConstant< T >", "class_sns_constant.html", [
            [ "SnsInput< T >", "class_sns_input.html", null ]
          ] ],
          [ "SnsDelay< T >", "class_sns_delay.html", null ],
          [ "SnsLogicAnd< T >", "class_sns_logic_and.html", null ],
          [ "SnsOutputCout< T >", "class_sns_output_cout.html", null ],
          [ "SnsSum< T >", "class_sns_sum.html", null ]
        ] ]
      ] ]
    ] ]
];