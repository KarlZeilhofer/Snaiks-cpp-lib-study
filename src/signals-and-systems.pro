TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    test.cpp \
    snsclockmaster.cpp \
    snssystem.cpp \
    snsconverter.cpp \
    snsnumeric.cpp \
    snssum.cpp \
    snsclockdevider.cpp \
    snsmemorymanager.cpp \
    snsconstant.cpp \
    snsinput.cpp \
    snsschmitttrigger.cpp \
    snslogicand.cpp \
    snshybrid.cpp \
    snsoutputcout.cpp

HEADERS += \
    snsclockmaster.h \
    snssystem.h \
    snsconverter.h \
    snsnumeric.h \
    snssum.h \
    snsclockdevider.h \
    snsmemorymanager.h \
    snsconstant.h \
    snsinput.h \
    snsschmitttrigger.h \
    snslogicand.h \
    snshybrid.h \
    snsoutputcout.h
