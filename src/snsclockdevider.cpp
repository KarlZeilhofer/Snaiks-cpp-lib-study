#include "snsclockdevider.h"


SnsClockDevider::SnsClockDevider(uint8_t maxSlaves, uint32_t devisor)
	:SnsClockMaster(maxSlaves), devisor(devisor)
{
	if(devisor == 0){
		// TODO: exception/error
	}
	counter = devisor;

}

void SnsClockDevider::process()
{
	counter--;

	if(counter==0){
		counter = devisor;
		this->processAllSlaves();
	}
}
