#ifndef SNSCLOCKDEVIDER_H
#define SNSCLOCKDEVIDER_H

#include "snsclockmaster.h"

/*!
 * \brief Sub-Class of ClockMaster, frequency reduction
 *
 * call process() n times
 * TODO: what about sampling?
 * status: untested
 */
class SnsClockDevider : public SnsClockMaster
{
public:
	SnsClockDevider(uint8_t maxSlaves, uint32_t devisor);

	void process();

protected:

private:
	uint32_t devisor; //! di
	uint32_t counter;
};

#endif // SNSCLOCKDEVIDER_H
