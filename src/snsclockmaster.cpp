#include "snsclockmaster.h"
#include "snsmemorymanager.h"



SnsClockMaster::SnsClockMaster(uint8_t maxNSlaves)
	:maxNSlaves(maxNSlaves)
{
	// allocate memory for the object pointers
	pSlaves = (SnsSystem**) SnsMemoryManager::allocate(sizeof(SnsSystem*)*maxNSlaves);
	nSlaves = 0;
}

/*!
 * \brief Reset all Clock Slaves
 */
void SnsClockMaster::reset()
{
	for(int n=0; n<nSlaves; n++){
		pSlaves[n]->reset();
	}
}

void SnsClockMaster::sample()
{
	sampleAllSlaves();
}

void SnsClockMaster::process()
{
	processAllSlaves();
}

/*!
 * \brief SnsClockMaster::addSlave
 * \param slave
 * \return address of added slave, or 0 if array is full
 */
SnsSystem* SnsClockMaster::addSlave(SnsSystem *slave)
{
	if(nSlaves < maxNSlaves){
		pSlaves[nSlaves++]=slave;
		return slave;
	}else{
		return 0;
	}
}

/*!
 * \brief TODO
 * \param slave
 */
void SnsClockMaster::removeSlave(SnsSystem *slave)
{
	// TODO, if needed
}

void SnsClockMaster::sampleAllSlaves()
{
	// TODO: converters first???
	for(int n=0; n<nSlaves; n++){
		pSlaves[n]->sample();
	}
}

void SnsClockMaster::processAllSlaves()
{
	for(int n=0; n<nSlaves; n++){
		pSlaves[n]->process();
	}
}
