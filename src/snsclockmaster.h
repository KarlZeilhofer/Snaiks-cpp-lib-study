#ifndef SNS_CLOCKMASTER_H
#define SNS_CLOCKMASTER_H


#include "snssystem.h"

/*!
 * \brief A clock source for any system-sub-class
 *
 * A ClockMaster holds a number of clock slaves (SnsSystem) \n
 * SnsClockMaster::process() must be called by the main programm, or by another ClockMaster
 */
class SnsClockMaster: public SnsSystem
{
public:
	SnsClockMaster(uint8_t maxNSlaves);

	void reset();
	/*!
	 * \brief Make a time step: sample()
	 *
	 * sample() calls for each SnsSystem in clockSlaves first SnsSystem::sample()
	 */
	void sample();
	/*!
	 * \brief Make a time step: process()
	 *
	 * process() calls for each SnsSystem in clockSlaves then for each system SnsSystem::process()
	 */
	void process();

	SnsSystem* addSlave(SnsSystem* slave);
	void removeSlave(SnsSystem* slave);

protected:
	void sampleAllSlaves();
	void processAllSlaves();

	/*!
	 * \brief Array of pointers to different system objects.
	 *
	 * Here we collect all the objects, which get the same clock signal.
	 */
	SnsSystem** pSlaves;
	int nSlaves; //!< number of slaves.
	int maxNSlaves;


};

#endif // SNS_CLOCKMASTER_H
