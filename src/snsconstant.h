#ifndef SNSCONSTANT_H
#define SNSCONSTANT_H

#include "snsnumeric.h"

/*!
 * \brief Numeric, with one constant output.
 *
 * Specify init0 in KiCad
 */
template <typename T>
class SnsConstant : public SnsNumeric<T>
{
public:
	SnsConstant(T value);
	void sample();
	void process();
};



/*
 * Template Member-Functions:
 */


	template <typename T>
	SnsConstant<T>::SnsConstant(T value)
	{
		this->setupArrays(0,1);
		this->outputs[0] = value;
	}

	template <typename T>
	void SnsConstant<T>::sample()
	{
		// nothing to do here.
	}

	template <typename T>
	void SnsConstant<T>::process()
	{
		// nothing to do here.
	}


#endif // SNSCONSTANT_H
