#ifndef SNSCONVERTER_H
#define SNSCONVERTER_H

#include "snssystem.h"

/*!
 * \brief Type conversion with zero delay.
 *
 * Specify type1 and type2 in KiCad
 * Or none, auto-detected...
 */
template <typename T_From, typename T_To>
class SnsConverter : public SnsSystem
{
public:
	SnsConverter();
	void convert();
	void sample();
	void process();

	void setSource(T_From* src);
	T_To* getOutput();

private:
	T_From* pInput;
	T_To output;
};


template <typename T_From, typename T_To>
void SnsConverter<T_From, T_To>::convert()
{
	output = *pInput;
}

template <typename T_From, typename T_To>
void SnsConverter<T_From,T_To>::sample()
{

}

template <typename T_From, typename T_To>
void SnsConverter<T_From,T_To>::process()
{

}


template <typename T_From, typename T_To>
SnsConverter<T_From, T_To>::SnsConverter()
{
	output = 0;
	pInput=0;
}



#endif // SNSCONVERTER_H
