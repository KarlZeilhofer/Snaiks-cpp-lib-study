#ifndef SNSDELAY_H
#define SNSDELAY_H

#include "snsnumeric.h"

/*!
 * \brief Numeric system, which implements a one-clock-delay
 */
template <typename T>
class SnsDelay : public SnsNumeric<T>
{
public:
	SnsDelay();
	void sample();
	void process();

};



/*
 * Template Member Functions:
 */
	template <typename T>
	SnsDelay<T>::SnsDelay()
	{
		this->setupArrays(1,1);
		this->outputs[0]=0;
	}

	template <typename T>
	void SnsDelay<T>::sample()
	{
		this->inputs[0] = *(this->pInputs[0]);
	}

	template <typename T>
	void SnsDelay<T>::process()
	{
		this->outputs[0] = this->inputs[0];
	}


#endif // SNSDELAY_H
