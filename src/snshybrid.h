#ifndef SNSHYBRID_H
#define SNSHYBRID_H

#include "snssystem.h"
#include "snsmemorymanager.h"

/*!
 * \brief Interface Class for Systems with different Input-/Output-Types
 *
 */

template <typename T1, typename T2>
class SnsHybrid : public SnsSystem
{
public:
	SnsHybrid();

	void reset(); //! implements default
	void convert(){} //! do nothing by default
	void sample();
	// void process(); // must be implemented in sub-class

	void setSource(uint8_t inputID, T1* src);
	T2* getOuput(uint8_t outputID);

	T1** pInputs; //!< array of input pointers.
	T1* inputs; //!< array of sampled inputs
	T2* outputs; //!< array of outputs
	uint8_t nInputs; //!< number of input pointers
	uint8_t nOutputs; //!< number of outputs

protected:
	void setupArrays(uint8_t nIn, uint8_t nOut);
};





/*
 * Template Member-Functions:
 */

	template <typename T1, typename T2>
	SnsHybrid<T1,T2>::SnsHybrid()
	{
		nInputs=0;
		nOutputs=0;
	}


	/*!
	 * \brief set all inputs and outputs by default to 0
	 *
	 * TODO: does this make sense for any type or class?
	 */
	template <typename T1, typename T2>
	void SnsHybrid<T1,T2>::reset()
	{
		for(uint8_t n=0; n<nInputs; n++){
			inputs[n] = 0;
		}
		for(uint8_t n=0; n<nOutputs; n++){
			outputs[n] = 0;
		}
	}

	/*!
	 * \brief Copy values from the sources into this object
	 *
	 * This step is needed to make the execution of a whole network of systems
	 * independent of the order of execution.
	 */
	template <typename T1, typename T2>
	void SnsHybrid<T1,T2>::sample()
	{
		for(uint8_t n=0; n<nInputs; n++){
			T1 h = *pInputs[n];
			inputs[n] = h;
		}
	}

	template <typename T1, typename T2>
	void SnsHybrid<T1, T2>::setupArrays(uint8_t nIn, uint8_t nOut)
	{
		nInputs = nIn;
		nOutputs = nOut;

		pInputs = 0;
		inputs = 0;
		outputs = 0;

		if(nIn){
			pInputs = (T1**) SnsMemoryManager::allocate(sizeof(T1*)*nIn);
			inputs = (T1*) SnsMemoryManager::allocate(sizeof(T1)*nIn);
		}
		if(nOut){
			outputs = (T2*) SnsMemoryManager::allocate(sizeof(T2)*nOut);
		}
	}


	template <typename T1, typename T2>
	void SnsHybrid<T1,T2>::setSource(uint8_t inputID, T1* src)
	{
		pInputs[inputID] = src;
		// TODO: check index
	}

	template <typename T1, typename T2>
	T2* SnsHybrid<T1,T2>::getOuput(uint8_t outputID)
	{
		return &(outputs[outputID]);
		// TODO: check index
	}



#endif // SNSHYBRID_H
