#ifndef SNSINPUT_H
#define SNSINPUT_H

#include "snsconstant.h"

/*!
 * \brief Numeric signal source. Use setValue()
 *
 * Provides no input-pins and one output-pin
 */
template <typename T>
class SnsInput: public SnsConstant<T>
{
public:
	SnsInput(T value);

	inline void setValue(T value);
};




/*
 * Template Member-Functions:
 */

	template <typename T>
	SnsInput<T>::SnsInput(T value)
		:SnsConstant<T>(value)
	{

	}

	template <typename T>
	void SnsInput<T>::setValue(T value)
	{
		this->outputs[0] = value;
	}

#endif // SNSINPUT_H
