#ifndef SNSLOGICAND_H
#define SNSLOGICAND_H

#include "snsnumeric.h"

/*!
 * \brief Logic AND gate with 2 inputs
 */
template <typename T>
class SnsLogicAnd : public SnsNumeric<T>
{
public:
	SnsLogicAnd();

	void process();
};


/*
 * Template Member-Functions:
 */
	template<typename T>
	SnsLogicAnd<T>::SnsLogicAnd()
	{
		this->setupArrays(2,1);
		this->outputs[0] = 0;
	}

	template<typename T>
	void SnsLogicAnd<T>::process()
	{
		this->outputs[0] = this->inputs[0] && this->inputs[1];
	}

#endif // SNSLOGICAND_H
