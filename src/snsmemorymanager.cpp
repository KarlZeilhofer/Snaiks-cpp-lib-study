#include "snsmemorymanager.h"

static uint8_t __sns_memory[SNS_MEMORY_MANAGER_SIZE];

uint8_t* SnsMemoryManager::nextFreeAddress = __sns_memory;
uint8_t* SnsMemoryManager::memory = __sns_memory;

void* SnsMemoryManager::allocate(uint32_t size)
{
	if((unsigned int)(nextFreeAddress-memory) + size >= SNS_MEMORY_MANAGER_SIZE){
		return 0;
	}else{
		void* ret = nextFreeAddress;
		nextFreeAddress+=size;
		return ret;
	}
}
