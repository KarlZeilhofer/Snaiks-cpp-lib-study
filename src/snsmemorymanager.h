#ifndef SNSMEMORYMANAGER_H
#define SNSMEMORYMANAGER_H

#include "snssystem.h"

#define SNS_MEMORY_MANAGER_SIZE (16*1024)

/*!
 * \brief The SnsMemoryManager class is static and implements a memory management
 *
 * Since its intended use is to allocate memory for all the objects, wben building
 * the network of systems, we hav no function to free memory. This also helps
 * to avoid memory fragemntation.
 */
class SnsMemoryManager
{
public:
	static void *allocate(uint32_t size);

private:
	static uint8_t* nextFreeAddress;
	static uint8_t* memory;
};

#endif // SNSMEMORYMANAGER_H
