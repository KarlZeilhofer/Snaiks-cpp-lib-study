#ifndef SNSNUMERIC_H
#define SNSNUMERIC_H

#include "snshybrid.h"

/*!
 * \brief Interface Class for Numeric Systems with identical Input-/Output-Types
 *
 */
template <typename T>
class SnsNumeric : public SnsHybrid<T,T>
{
public:
	SnsNumeric();
};




/*
 * Template Member-Functions:
 */

	template<typename T>
	SnsNumeric<T>::SnsNumeric()
		:SnsHybrid<T,T>::SnsHybrid()
	{

	}


#endif // SNSNUMERIC_H
