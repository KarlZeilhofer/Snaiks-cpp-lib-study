#ifndef SNSOUTPUTCOUT_H
#define SNSOUTPUTCOUT_H

#include "snsnumeric.h"
#include "iostream"

/*!
 * \brief Numeric Signal-Sink, which writes its value to std::cout
 */
template <typename T>
class SnsOutputCout : public SnsNumeric<T>
{
public:
	SnsOutputCout();
	void process();
};


/*
 * Template Member-Functions:
 */
	template <typename T>
	SnsOutputCout<T>::SnsOutputCout()
	{
		this->setupArrays(1,0);
	}

	template <typename T>
	void SnsOutputCout<T>::process()
	{
		std::cout << this->inputs[0] << " ";
	}


#endif // SNSOUTPUTCOUT_H
