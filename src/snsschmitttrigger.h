#ifndef SNSSCHMITTTRIGGER_H
#define SNSSCHMITTTRIGGER_H

#include "snsnumeric.h"

/*!
 * \brief Schmitt-Trigger block, with 3 analog inputs, and 2 digital outputs
 *
 * TODO: details
 */
template <typename T>
class SnsSchmittTrigger : public SnsHybrid<T, bool>
{
public:
	SnsSchmittTrigger();

	void process();
};



/*
 * Template Member-Functions:
 */

	template <typename T>
	SnsSchmittTrigger<T>::SnsSchmittTrigger()
	{
		this->setupArrays(3,2);
		this->outputs[0] = 0; // Q
		this->outputs[1] = 1; // Qn
	}

	template <typename T>
	void SnsSchmittTrigger<T>::process()
	{
		bool& Q = this->outputs[0];
		bool& Qn = this->outputs[1];

		T& upLim = this->inputs[0];
		T& in = this->inputs[1];
		T& lowLim = this->inputs[2];

		if(Q && in<lowLim){
			Q=false;
		}else if(Qn && in>upLim){
			Q=true;
		}else{
			// nothing to do here
		}

		Qn = !Q;
	}
#endif // SNSSCHMITTTRIGGER_H
