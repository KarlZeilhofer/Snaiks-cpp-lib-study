#ifndef SNSSUM_H
#define SNSSUM_H

#include "snsnumeric.h"

/*!
 * \brief Numerical summation of 2 input signals
 */
template <typename T>
class SnsSum : public SnsNumeric<T>
{
public:
	SnsSum();

	void process();

private:
	T* _pInputs[2];
	T _outputs[1];
};




/*
 * Template Member-Functions:
 */

	template<typename T>
	SnsSum<T>::SnsSum()
	{
		this->setupArrays(2,1); // we have to use this here, beause of this error:
			// http://eli.thegreenplace.net/2012/02/06/dependent-name-lookup-for-c-templates/
		this->outputs[0]=0;
	}

	template <typename T>
	void SnsSum<T>::process()
	{
		this->outputs[0] = *this->pInputs[0] + *this->pInputs[1];
	}


#endif // SNSSUM_H
