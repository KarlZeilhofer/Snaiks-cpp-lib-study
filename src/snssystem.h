#ifndef SNS_SYSTEM_H
#define SNS_SYSTEM_H

#include <stdint.h>

#define SNS_INFO_SYSTEM //!< informations such as className, objectName, socketName are optional
#undef SNS_INFO_SYSTEM

/*! \mainpage Snaiks - Signals and Systems from KiCad to C++
 *
 * \section intro_sec Introduction
 *
 * This C++ library is intended to be used either on normal computers or also
 * on microcontrollers without any operating system or memory management.
 *
 * It should simplify the creation of signal-structures and also logic
 * behaveour like in a programmable logic controller (PLC).
 *
 * \section kicad_sec Relations to KiCad Schematics
 * \subsection comp_ssec Kicad Component vs C++ Class
 *
 * Component | Example | Class | Example
 * ----------|---------|-------|--------
 * Reference | Const1 | char* SnsConstant::objectName | "Const1"
 * Value     | upperLimit | char* SnsConstant::description | "upperLimit"
 * Component | Constant | char* SnsConstant::className | "SnsConstant"
 *
 * In the following we use the example of the class SnsSchmittTrigger:SnsSystem
 *
 * Pin               | Value       | in C++
 * ------------------|-------------|--------------
 * name              | upperLimit | double SnsSchmittTrigger::upperLimit |
 * number            | i0 | index of array SnsInputSocket inputs[]
 * Electrical Type   | Input | -
 *        .           | . | .
 * name              | in | double SnsSchmittTrigger::in |
 * number            | i1 | index of array SnsInputSocket inputs[]
 * Electrical Type   | Input | -
 *        .           | . | .
 * name              | lowerLimit | double SnsSchmittTrigger::lowerLimit |
 * number            | i2 | index of array SnsInputSocket inputs[]
 * Electrical Type   | Input | -
 *        .           | . | .
 * name              | Q | double SnsSchmittTrigger::Q |
 * number            | o0 | index of array SnsOutputSocket outputs[]
 * Electrical Type   | Output | -
 *        .           | . | .
 * name              | Qn | double SnsSchmittTrigger::Qn |
 * number            | o1 | index of array SnsOutputSocket outputs[]
 * Electrical Type   | Output | -
 *        .           | . | .
 * name (constant)   | CLK | this object gets added to a SnsClockMaster |
 * number (constant) | ci | -
 * Electrical Type   | Input | -
 *
 *
 * TODO: make it possible, that clock lines can be combined with logic gates.
 * Concept:
 *	the wires which go from a ClockMaster to clock inputs _clk are of a completely
 *	different type than the other signal wires. The clock wires collect
 *  systems in an array, so the master knows, which object-functions to call, the
 *  other wires are realized by backward pointers (from input ot source).
 *
 * Let's assume, we have a KiCad component "ClockMaster". Its value is mainClock.
 * The we generate in C++ a SnsClockMaster mainClock. Every KiCad component, which is
 * connected to the output pin of mainClock, will in C++ be added to the array of
 * systems.
 *
 *
 * Notes on Template Member-Functions:
 * -----------------------------------
 *
 * According to
 * https://stackoverflow.com/questions/22595015/c-class-template-undefined-reference-to-function
 * these functions must be **defined** in the header file, and not in the cpp-file.
 */




/*!
 * \brief Abstract Class for the description of an input or output
 *
 * It is intended, that this class is not a template class. \n
 * Template class instances of different types use different memory space, and therefore they
 * cannot put together into a common array. But eaxactly this is needed in the systems.
 */

#ifdef SNS_INFO_SYSTEM
class SnsSocket
{
	char* name; //!< name of this input our output
	char* type; //!< type as string (e.g. int16, double, bool, ...)
	virtual void print()=0;
};

/*!
 * \brief Descriptor class for an input socket
 */
class SnsInputSocket:SnsSocket // extends Socket
{
	void* pSample; //!< pointer to the sampled (coppied) value.
	void* pSource; //!< pointer to the output, to which this input is connected to
};

/*!
 * \brief Description of an output socket
 */
class SnsOuputSocket:SnsSocket // extends Socket
{
	void* pOutput; //!< pointer to the member variable of a system, which holds the output value
};
#endif

/*!
 * \brief Abstract interface class for various sub-classes
 *
 * Any specific system must be derived from this abstract class.
 * A ClockMaster calls this meber functions in this order: convert() sample() and process()
 */
class SnsSystem
{
public:
	SnsSystem();
	~SnsSystem();

	/*!
	 * \brief Reset function
	 */
	void reset();

	/*!
	 * \brief conversion between different types
	 *
	 * Since we should be able to connect systems with different data tpyes,
	 * for example a scope, which normally accepts double values, should also be
	 * able to record digital signals (bool) or integers.
	 *
	 * We have here a separate function convert(), which doesn't introduce a time-delay.
	 * Otherwise we could have used also the process() function.
	 */
	void convert();

	/*!
	 * Samples the value from the input into a local class member
	 * this is needed, so that the order of calling process() of the
	 * different systems doesn't matter
	 */
	virtual void sample()=0; // abstract method

	/*!
	 * This function is called periodically by a ClockMaster
	 */
	virtual void process()=0; // abstract method

#ifdef SNS_INFO_SYSTEM
	void print();
#endif

protected:
#ifdef SNS_INFO_SYSTEM
	char* className; //!< fixed name, to identify this class, must be set in contructor
	char* objectName; //!< user defined name, to identify an instance of this class
	char* description; //!< decribes the function of this object

	SnsInputSocket** inputInfos; //!< array of inputs
	SnsOuputSocket** outputInfos; //!< array of outputs
	int nInputs; //!< number of inputs
	int nOutputs; //!< number of outputs
#endif
};

#endif // SNS_SYSTEM_H
